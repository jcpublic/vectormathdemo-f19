package com.example.bullettime;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "VECTOR-MATH";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // SPRITES
    Square bullet;
    Square bullet1;
    Square bullet2;
    Square bullet3;
    Square bullet4;
    Square bullet5;
    Square bullet6;

    // Array of Bullets
    ArrayList<Square> bullets = new ArrayList<Square>();


    Square enemy;

    int SQUARE_WIDTH = 100;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;



//        this.bullet = new Square(context, 100, 600, SQUARE_WIDTH);
        // initalize sprites
        // lets make 6 bullets
        int bulletStartX = 100;

        // make 10 bullets
        int baseSpeed = 2;
        for (int i = 1; i < 11; i++) {
            Square b = new Square(context, 100, 600, SQUARE_WIDTH, 2*i);
            this.bullets.add(b);
        }

        this.enemy = new Square(context, 1000, 100, SQUARE_WIDTH, 0);

    }

    @Override
    public void run() {
        // @TODO: Put game loop in here
        while (gameIsRunning == true) {
            updateGame();
            redrawSprites();
            controlFPS();
        }
    }

    public void moveBulletToTarget(Square bullet) {
        // @TODO:  Move the square
        // 1. calculate distance between bullet and square
        double a = (enemy.xPosition - bullet.xPosition);
        double b = (enemy.yPosition - bullet.yPosition);
        double distance = Math.sqrt((a*a) + (b*b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        bullet.xPosition = bullet.xPosition + (int)(xn * bullet.getSpeed());
        bullet.yPosition = bullet.yPosition + (int)(yn * bullet.getSpeed());
    }


    float mouseX;
    float mouseY;


    public void moveBulletToMouse(Square bullet, float mouseXPos, float mouseYPos) {
        // @TODO:  Move the square
        // 1. calculate distance between bullet and square
        double a = (mouseXPos - bullet.xPosition);
        double b = (mouseYPos - bullet.yPosition);
        double distance = Math.sqrt((a*a) + (b*b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        bullet.xPosition = bullet.xPosition + (int)(xn * bullet.getSpeed());
        bullet.yPosition = bullet.yPosition + (int)(yn * bullet.getSpeed());
    }




    public void spawnBullet() {

//        if (this.bullets.size() < 10) {
//            Square b;
//            if (this.bullets.isEmpty()) {
//                b = new Square(getContext(), 100, 600, SQUARE_WIDTH);
//            } else {
//                // prev bullet
//                Square prevBullet = this.bullets.get(this.bullets.size() - 1);
//                int newBulletX = prevBullet.xPosition + 120;
//                b = new Square(getContext(), newBulletX, 600, SQUARE_WIDTH);
//            }
//            this.bullets.add(b);
//        }

    }

    int count = 0;


    public enum Direction {
        UP,
        DOWN
    }

    final int BOTTOM_OF_SCREEN = 700;
    final int TOP_OF_SCREEN = 100;

    Direction bulletDirection = Direction.DOWN;

    // Game Loop methods
    public void updateGame() {

        // 1. MOVE THE TARGET (Purple square)
        if (bulletDirection == Direction.DOWN) {
            enemy.yPosition = enemy.yPosition + 10;

            if (enemy.yPosition > BOTTOM_OF_SCREEN) {
                bulletDirection = Direction.UP;
            }
        }
        if (bulletDirection == Direction.UP) {
            enemy.yPosition = enemy.yPosition - 10;

            if (enemy.yPosition < TOP_OF_SCREEN) {
                bulletDirection = Direction.DOWN;
            }
        }


        // MOVE YOUR BULLETS
        for (int i = 0; i < this.bullets.size();i++) {
            Square b = this.bullets.get(i);
            moveBulletToMouse(b, this.mouseX, this.mouseY);
        }




        // 2. MOVE THE BULLET (black square)



//        count = count + 1;
//        // spawn a new bullet every x seconds
//        if (count % 10 == 0) {
//            this.spawnBullet();
//        }
//
//
//        for (int i = 0 ; i < bullets.size(); i++) {
//            Square b = this.bullets.get(i);
//
//            b.yPosition = b.yPosition - 25;
//            //moveBullet(b);
//        }
//
//        // collision detection
//        for (int i = 0 ; i < bullets.size(); i++) {
//            Square b = this.bullets.get(i);
//
//            if (b.yPosition <= 0) {
//                b.yPosition = 600;
//            }
//        }


//        Log.d(TAG,"New bullet (x,y): (" + bullet.xPosition + "," + bullet.yPosition + ")");
    }

    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------
            // @TODO: put your drawing code in this section

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(8);

            // draw bullet
            paintbrush.setColor(Color.BLACK);

            for (int i = 0; i < this.bullets.size();i++) {
                Square b = this.bullets.get(i);
                canvas.drawRect(
                        b.getxPosition(),
                        b.getyPosition(),
                        b.getxPosition() + b.getWidth(),
                        b.getyPosition() + b.getWidth(),
                        paintbrush
                );
            }

//
//            // draw single bullet
//            canvas.drawRect(
//                    this.bullet.getxPosition(),
//                    this.bullet.getyPosition(),
//                    this.bullet.getxPosition() + this.bullet.getWidth(),
//                    this.bullet.getyPosition() + this.bullet.getWidth(),
//                    paintbrush
//            );




            // draw enemy
            paintbrush.setColor(Color.MAGENTA);
            canvas.drawRect(
                    this.enemy.getxPosition(),
                    this.enemy.getyPosition(),
                    this.enemy.getxPosition() + this.enemy.getWidth(),
                    this.enemy.getyPosition() + this.enemy.getWidth(),
                    paintbrush
            );


            // show how many buillets you have
            paintbrush.setTextSize(60);
            canvas.drawText("Bullets: " + this.bullets.size(), 1000, 600, paintbrush);




            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            //gameThread.sleep(17);
            gameThread.sleep(50);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_DOWN:
                this.mouseX = event.getX();
                this.mouseY = event.getY();
                break;
        }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

